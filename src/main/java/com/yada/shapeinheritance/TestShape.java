/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.shapeinheritance;

/**
 *
 * @author ASUS
 */
public class TestShape {
    public static void main(String[] args) {
        Circle circle1 = new Circle(3);
        circle1.print();
        
        Circle circle2 = new Circle(2);
        circle2.print();
        
        Triangle triangle = new Triangle(4,3);
        triangle.print();
        
        Rectangle rectangle = new Rectangle(3,4);
        rectangle.print();
        
        Square square = new Square(2);
        square.print();
        
        System.out.println("******");
        
        Shape[] shape = {circle1,circle2,triangle,rectangle,square};
        for(int i=0; i<shape.length;i++) {
            shape[i].print();
        }
    }
}
